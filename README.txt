/* README.txt */


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Installation
 * How to Use
 * Frequently Asked Questions (FAQ)
 * Known Issues
 * Author
 
INTRODUCTION
------------
 
Fieldrating is a field module that provides a rating field and display formatter using Drupal 7's Field Type API.  This field module was 
created to support a simple ratings field that can be used on any content type.  This module is not like Fivestar and does NOT support user
voting or rating.  But instead can be used for rating content, products, locations, etc.

FEATURES
------------
* Supports multiple rating values
* Icon (graphic files) can be of any size. Icon file is two graphic images stacked on top of each other. Top image is the gray image and the bottom is the active rating image.
* Several icon files come with this module. Other icons can be kept in your current theme images/ratingsfield directory.
* You can have a label text field for each rating.
* You can have a suffix text field for each rating.
* You can show text if the rating has not yet been done.
* You can have nothing show up if the rating has not been done.
* Each of the elements (icons, text, whole field) is customizable with css and support custom css classes.
* Simple field development with only core dependancies.
* Utilizes Drupal 7's Field API.
* Can display text or icons for ratings


INSTALLATION
------------
This module is installed in a similar fashion to any other module.  Extract the archive file
in the /sites/all/modules or /sites/domain/modules directory.  Then enable the module from Modules administration.
If you have any questions or have not done this before, then go to http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

HOW TO USE
==========
This module provides you with a new field when you are creating fields in "Content types".  
Example:  If you are adding fields to a new "Content type" called "Resturants", then you may wish to have a ratings available for the published content.

1)  You would go to the admin menu and choose "Structure" and then "Content types". 
2)  Now click on the "manage fields" selection under "Operations".  
3)  At the bottom of this form you will find a set of textboxes and selectors under "Add new field".
4)  Type in a Label : This will be the label (name) that is used to identify the ratings.
5)  Enter in the machine name for the field.
6)  Select "Ratings" for the field type.
7)  You have two choices for widgets
     a)  Select Entry - this is the default and it will give you a selector to set the rating that is easy to use
     b)  Text Entry - this will provide a text field for entry.  You have to remember some simple rules about range and value if you use this.
8)  Click on Save and your field has now beed added.  

You will now get a "settings" page that will ask you to review or set some options.

9)  Label and Help text are standard with any field entry.
10) Checkbox - Fill with light or grey icons.  If you check this, it will use the "grey" or top graphic in the icon file to fill out the maximum
      rating of your display.  If this feature is OFF (unchecked), then you will get two(2) icons displayed for a rating of "2".  If this is checked
      then you will get two (2) icons for the rating and then three (3) gray icons after it if the maximum rating is 5.
11) Icon filename - this provides you with a way to choose and select the graphic icon file you are using to display the rating. 
      This will list out the graphic files in the modules/ratingsfield/icons directory or in your current theme's images/ratingsfield directory.
12) Text to print when Rating is empty - this is pretty self explainatory.
13) Maximum Rating Value - choose from a range from 1 to 10.  This is used to check ratings value and also to display light icons when the 
       "Fill In" is checked.
14) Default Value - Rating : enter in a value if you want this to be the general default for the rating values.
15) Default Value - Rating Label : a value that is used to display a label in front of the rating.
16) Default Value - Rating Label Suffix: a value that is used to display a label after the rating.
17) Number of values - you can create multiple values of ratings, each with its own Label and Label Suffix.
18) Save

Manage Display - you can select the format of the ratings display for Teasers, Default, or Custom display settings.

19)  You will see the Ratingsfield field listed in the fields on the left.  Follow the FORMAT column down and you will find a selection
     box that reads, "Ratings Default".  You have three choices on this selection.
20) Ratings Default - show the icons as selected in the ratingsfield settings.  The display will be organized as follows

(ratingsfield label)
(rating label) (ratings icons) (gray icons if selected) (ratings label suffix)
.... repeating for number of values.

21) Ratings Text Only - show a text version of the ratings.  The display will be organized as follows.
(ratingsfield label)
(rating label) (rating value) (ratings label suffix)
.... repeating for number of values.

22) Hidden - not displayed.

If there is no rating then nothing will be displayed.
If the rating has the value of "-1" then it will display the "empty"/"no rating" text.


FREQUENTLY ASKED QUESTIONS
--------------------------
Q: Why is there a limit on the range of ratings from 0 to 10?
A: The module is built to handle a wider range but additional settings and logic will need to be added to factor
    the range into a reasonable icon display.

Q: What about browser readers or copy/paste functions when displaying icons.
A: The ratings label and values are rended on the page in "hidden" text so that real values can be available.

KNOWN ISSUES
------------
Since this is built using the Field API it will only work on Drupal 7.
There are no known issues at this time.


Author
======
Ralph Seibert (lagpro)
http://www.ralphseibert.com