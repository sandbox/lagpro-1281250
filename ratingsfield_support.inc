<?php
/**
 * @file
 * Support functions for the Ratingsfield Field object
 * 
 */
  
function ratingfield_get_iconlist() {
        $iconlistdir = array();
        $iconlistdir[ ] = drupal_get_path('module', 'ratingsfield') . '/icons';
        if (file_exists(drupal_get_path('theme', variable_get('theme_default', 'none')) . '/images/ratingsfield')) {
            $iconlistdir[ ] = drupal_get_path('theme', variable_get('theme_default', 'none')) . '/images/ratingsfield';
            }
        $iconfiles = array();
        $iconfilesdetail = array();
    foreach ($iconlistdir as &$icondir) {
        
    $icondirpath = dir($icondir);    

    while (($file = $icondirpath->read()) !== FALSE) {  
      $fullpath = $icondir . '/' . $file;
      if ((substr($file, -3)=="gif") || (substr($file, -3)=="jpg") || (substr($file, -3)=="png")) {
        $ifiletrim = trim($file);
        $iconfiles[ ] = $ifiletrim;   
        //$iconfiles[ ] = $file;   
        list($imgwidth, $imgheight, $imgtype, $imgattr) = getimagesize($fullpath);
       // $iconfilesdetail[ ]  = $fullpath . '/' . '|' . $ifiletrim . '|' . $imgwidth . '|' . $imgheight;   
        $iconfilesdetail[ ]  = $fullpath . '/' . '|' . $ifiletrim . '|' . $imgwidth . '|' . $imgheight;      
      }
    }
    $icondirpath->close();
    }

    $combinearray = array_combine($iconfilesdetail, $iconfiles);

    return $combinearray;
    }
        
function ratingfield_get_iconfiledetails($iconfile) {
        list($imgwidth, $imgheight, $imgtype, $imgattr) = getimagesize($iconfile);
        $patharray = pathinfo($iconfile);
        $ifiletrim = $patharray['basename'];
        $icondir = $patharray['dirname'];
        $iconfilesdetail = $icondir . '/' . '|' . $ifiletrim . '|' . $imgwidth . '|' . $imgheight;        
    return $iconfilesdetail;
    }

/**
 *  Debugging function 
 */
function ratingsfield_debug($str) {
  if (is_array($str) || is_object($str)) {

    error_log(print_r($str, TRUE) . "\r\n", 3, "/tmp/0001ratingsfield.txt");
  }
  else {
    error_log($str . "\r\n", 3, "/tmp/0001ratingsfield.txt");
  }
}
